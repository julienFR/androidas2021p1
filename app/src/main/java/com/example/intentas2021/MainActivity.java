package com.example.intentas2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openUrl(View view) {
        EditText editText = findViewById(R.id.edtURL);
        String url = editText.getText().toString();
        openWebPage("https://"+url);
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void callNumber(View view) {
        EditText editText = findViewById(R.id.edtTel);
        String tel = editText.getText().toString();
        dialPhoneNumber(tel);
    }

    public void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void takePicture(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent,1);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (resultCode == RESULT_OK){
            if (requestCode == 1) {
                Bitmap thumbnail = data.getParcelableExtra("data");
                ImageView imageView = findViewById(R.id.imageView);
                imageView.setImageBitmap(thumbnail);
            }else if (requestCode == 2){
                String reponse = data.getStringExtra("reponse");
                String planette = data.getStringExtra("planette");
                TextView textView = findViewById(R.id.tvRepAct2);
                textView.setText("Reponse : " + reponse + " " + planette);
            }
        }

    }

    public void openActivityTwo(View view) {
        EditText editText = findViewById(R.id.edtMessage);
        String message = editText.getText().toString();
        Intent intent = new Intent(this,MainActivity2.class);
        intent.putExtra("message",message);
        startActivityForResult(intent,2);
    }
}
