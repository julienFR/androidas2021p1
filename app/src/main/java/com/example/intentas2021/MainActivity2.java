package com.example.intentas2021;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        String message = intent.getStringExtra("message");
        TextView textView = findViewById(R.id.tvMessageActivite2);
        textView.setText(message);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    @Override
    public void finish(){
        Intent intent = new Intent();
        EditText editText = findViewById(R.id.edtReponse);
        String reponse = editText.getText().toString();
        intent.putExtra("reponse",reponse);
        Spinner spinner = findViewById(R.id.spinner);
        String planette = spinner.getSelectedItem().toString();
        Log.d("MesLogs",planette);
        intent.putExtra("planette",planette);
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }

    public void backToMainActivity(View view) {
        finish();
    }
}